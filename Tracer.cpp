#include "Tracer.hpp"
#include "Constants.hpp"

Tracer::Tracer()
	: world_ptr{nullptr}
{}

Tracer::Tracer(World * w)
	: world_ptr{w}
{}

Tracer::~Tracer()
{}

RGBColor Tracer::trace_ray(Ray const & ray) const
{
	return black;
}

RGBColor Tracer::trace_ray(Ray const & ray, int depth) const
{
	return black;
}
