#include "RGBColor.hpp"
#include <cmath>

RGBColor::RGBColor()
	: r{0}, g{0}, b{0}
{}

RGBColor::RGBColor(float c)
	: r{c}, g{c}, b{c}
{}

RGBColor::RGBColor(float r, float g, float b)
	: r{r}, g{g}, b{b}
{}

RGBColor::RGBColor(RGBColor const & c)
	: r{c.r}, g{c.g}, b{c.b}
{}

RGBColor & RGBColor::operator=(RGBColor const & rhs)
{
	if (this == &rhs)
		return *this;
	
	r = rhs.r;
	g = rhs.g;
	b = rhs.b;
	
	return *this;
}

RGBColor::~RGBColor()
{}

RGBColor RGBColor::powc(float p) const
{
	return RGBColor(pow(r,p), pow(g,p), pow(b,p));
}

