#pragma once
#include "GeometricObject.hpp"
#include "Point3D.hpp"

class Sphere : public GeometricObject
{
public:
	Sphere();
	Sphere(Point3D const & c, double r);
	Sphere(Sphere const & rhs);
	Sphere * clone() const override;
	Sphere & operator=(Sphere const & rhs);
	~Sphere();

	void set_center(Point3D const & c);
	void set_center(double x, double y, double z);
	void set_radius(double r);
	bool hit(Ray const & ray, double & tmin, ShadeRec & sr) const override;

private:
	Point3D center;  //!< sphere center
	double radius;  //!< sphere radius

	static double const kEpsilon;  //!< for shadows and secondary rays
};



inline void Sphere::set_center(Point3D const & c)
{
	center = c;
}

inline void Sphere::set_center(double x, double y, double z)
{
	center.x = x;
	center.y = y;
	center.z = z;
}

inline void Sphere::set_radius(double r)
{
	radius = r;
}
