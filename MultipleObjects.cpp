#include "MultipleObjects.hpp"
#include "World.hpp"

MultipleObjects::MultipleObjects()
{}

MultipleObjects::MultipleObjects(World * w)
	: Tracer{w}
{}

MultipleObjects::~MultipleObjects()
{}

RGBColor MultipleObjects::trace_ray(Ray const & ray) const
{
	ShadeRec sr{world_ptr->hit_bare_bones_objects(ray)};

	if (sr.hit_an_object)
		return sr.color;
	else
		return world_ptr->background_color;
}
