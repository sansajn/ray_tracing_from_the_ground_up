#pragma once
#include "Tracer.hpp"

class SingleSphere : public Tracer
{
public:
	SingleSphere();
	SingleSphere(World * w);
	~SingleSphere();

	RGBColor trace_ray(Ray const & ray) const override;
};
