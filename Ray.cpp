#include "Ray.hpp"

Ray::Ray()
	: o{0}, d{0, 0, 1.0}
{}

Ray::Ray(Point3D const & origin, Vector3D const & dir)
	: o{origin}, d{dir}
{}

Ray::Ray(Ray const & ray)
	: o{ray.o}, d{ray.d}
{}

Ray & Ray::operator=(Ray const & rhs)
{
	if (this == &rhs)
		return *this;
	
	o = rhs.o;
	d = rhs.d;
	
	return *this;
}

Ray::~Ray()
{}
