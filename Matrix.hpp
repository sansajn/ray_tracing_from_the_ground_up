#pragma once

class Matrix
{
public:
	double m[4][4];  //!< matrix elements
	
	Matrix();
	Matrix(Matrix const & mat);
	Matrix & operator=(Matrix const & rhs);
	~Matrix();
	
	Matrix operator*(Matrix const & mat) const;
	Matrix operator/(double d) const;
	void set_identity();
};
