#pragma once

#include "Matrix.hpp"
#include "Vector3D.hpp"

class Point3D
{
public:
	double x, y, z;
	
	Point3D();
	Point3D(double a);
	Point3D(double x, double y, double z);
	Point3D(Point3D const & p);
	Point3D & operator=(Point3D const & rhs);
	~Point3D();
	
	Point3D operator-() const;  //!< unary minus
	Point3D operator+(Point3D const & p) const;  //!< unary minus
	Point3D operator+(Vector3D const & v) const;
	Point3D operator-(Point3D const & p) const;
	Point3D operator-(Vector3D const & v) const;
	Point3D operator*(double a) const;
	double d_squared(Point3D const & p) const;  //!< square of distance between two points
	double distance(Point3D const & p) const;  //!< disance between two points
};


inline Point3D Point3D::operator-() const
{
	return Point3D{-x, -y, -z};
}

inline Point3D Point3D::operator+(Point3D const & p) const
{
	return Point3D{x+p.x, y+p.y, z+p.z};
}

inline Point3D Point3D::operator+(Vector3D const & v) const
{
	return Point3D{x+v.x, y+v.y, z+v.z};
}

inline Point3D Point3D::operator-(Point3D const & p) const
{
	return Point3D{x-p.x, y-p.y, z-p.z};
}

inline Point3D Point3D::operator-(Vector3D const & v) const
{
	return Point3D{x-v.x, y-v.y, z-v.z};
}

inline Point3D Point3D::operator*(double a) const
{
	return Point3D{a*x, a*y, a*z};
}

inline double Point3D::d_squared(Point3D const & p) const
{
	double dx = x - p.x;
	double dy = y - p.y;
	double dz = z - p.z;
	return dx*dx + dy*dy + dz*dz;
}

//! multiplication by a double on the left
inline Point3D operator*(double a, Point3D const & p)
{
	return Point3D{a*p.x, a*p.y, a*p.z};
}

//! multiplication by a matrix on the left
Point3D operator*(Matrix const & mat, Point3D const & p);
