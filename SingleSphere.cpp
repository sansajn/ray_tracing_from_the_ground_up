#include "SingleSphere.hpp"
#include "Constants.hpp"
#include "ShadeRec.hpp"
#include "World.hpp"

SingleSphere::SingleSphere()
{}

SingleSphere::SingleSphere(World * w)
	: Tracer{w}
{}

SingleSphere::~SingleSphere()
{}

RGBColor SingleSphere::trace_ray(Ray const & ray) const
{
	ShadeRec sr{*world_ptr};
	double t;
	if (world_ptr->sphere.hit(ray, t, sr))
		return red;
	else
		return black;
}
