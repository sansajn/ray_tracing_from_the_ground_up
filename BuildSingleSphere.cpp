// vygeneruje obrazok 3.18 s kapitoly 3
#include "World.hpp"
#include "Constants.hpp"
#include "SingleSphere.hpp"

void World::build()
{
	vp.set_hres(200);
	vp.set_vres(200);
	vp.set_pixel_size(1.0);
	vp.set_gamma(1.0);

	background_color = white;
	tracer_ptr = new SingleSphere{this};

	sphere.set_center(0);
	sphere.set_radius(85.0);
}
