#pragma once

class RGBColor
{
public:
	float r, g, b;
	
	RGBColor();
	RGBColor(float c);
	RGBColor(float r, float g, float b);
	RGBColor(RGBColor const & c);
	RGBColor & operator=(RGBColor const & rhs);
	~RGBColor();
	
	RGBColor operator+(RGBColor const & c) const;
	RGBColor & operator+=(RGBColor const & c);
	RGBColor operator*(float const a) const;
	RGBColor operator*(RGBColor const & c) const;
	RGBColor & operator*=(float const a);
	RGBColor & operator*=(RGBColor const & c);
	RGBColor operator/(float const a) const;
	RGBColor & operator/=(float const a);
	bool operator==(RGBColor const & c) const;
	RGBColor powc(float p) const;
	float average() const;
};

inline RGBColor RGBColor::operator+(RGBColor const & c) const
{
	return (RGBColor{r+c.r, g+c.g, b+c.b});
}

inline RGBColor & RGBColor::operator+=(RGBColor const & c)
{
	r += c.r;
	g += c.g;
	b += c.b;
	return *this;
}

inline RGBColor RGBColor::operator*(float const a) const
{
	return RGBColor{a*r, a*g, a*b};
}

inline RGBColor RGBColor::operator*(RGBColor const & c) const
{
	return RGBColor{r*c.r, g*c.g, b*c.b};
}

inline RGBColor & RGBColor::operator*=(float const a)
{
	r *= a;
	g *= a;
	b *= a;
	return *this;
}

inline RGBColor & RGBColor::operator*=(RGBColor const & c)
{
	r *= c.r;
	g *= c.g;
	b *= c.b;
	return *this;
}

inline RGBColor RGBColor::operator/(float const a) const
{
	return RGBColor{r/a, g/a, b/a};
}

inline RGBColor & RGBColor::operator/=(float const a)
{
	r /= a;
	g /= a;
	b /= a;
	return *this;
}

inline bool RGBColor::operator==(RGBColor const & c) const
{
	return (r==c.r && g==c.g && b==c.b);
}

inline float RGBColor::average() const
{
	return 0.333333333333 * (r+g+b);
}

// left float multiplication
inline RGBColor operator*(float const a, RGBColor const & c)
{
	return RGBColor{a*c.r, a*c.g, a*c.b};
}
