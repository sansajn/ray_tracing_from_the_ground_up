#include "World.hpp"
#include <algorithm>
#include "Constants.hpp" 
#include "Sphere.hpp"
#include "SingleSphere.hpp"
#include "Vector3D.hpp"
#include "Point3D.hpp"
#include "Normal.hpp"
#include "ShadeRec.hpp"
#include "wxraytracer.hpp"

using std::max;

World::World()
	: background_color{black}, tracer_ptr{nullptr}, paintArea{nullptr}
{}

World::~World()
{
	delete tracer_ptr;
	delete_objects();
}

void World::render_scene() const
{
	double zw = 100.0;
	Ray ray;
	ray.d = Vector3D{0, 0, -1};

	for (int r = 0; r < vp.vres; ++r)
	{
		for (int c = 0; c <= vp.hres; ++c)
		{
			double x = vp.s * (c - 0.5*(vp.hres - 1.0));
			double y = vp.s * (r - 0.5*(vp.vres - 1.0));
			ray.o = Point3D{x, y, zw};
			RGBColor pixel_color = tracer_ptr->trace_ray(ray);
			display_pixel(r, c, pixel_color);
		}
	}
}

RGBColor World::max_to_one(RGBColor const & c) const
{
	float max_value = max(c.r, max(c.g, c.b));
	if (max_value > 1.0)
		return c/max_value;
	else
		return c;
}

RGBColor World::clamp_to_color(RGBColor const & c) const
{
	if (c.r > 1.0 || c.g > 1.0 || c.b > 1.0)
		return RGBColor{1.0, 0.0, 0.0};
	else
		return c;
}

void World::display_pixel(int const row, int const column, RGBColor const & pixel_color) const
{
	RGBColor mapped_color;

	if (vp.show_out_of_gamut)
		mapped_color = clamp_to_color(pixel_color);
	else
		mapped_color = max_to_one(pixel_color);

	paintArea->setPixel(column, vp.vres - row - 1, 
		(int)(mapped_color.r * 255), (int)(mapped_color.g * 255), (int)(mapped_color.b * 255));
}

ShadeRec World::hit_bare_bones_objects(Ray const & ray)
{
	ShadeRec sr{*this};
	float tmin = kHugeValue;

	for (size_t j = 0; j < objects.size(); ++j)
	{
		double t;
		if (objects[j]->hit(ray, t, sr) && (t < tmin))
		{
			sr.hit_an_object = true;
			tmin = t;
			sr.color = objects[j]->get_color();
		}
	}

	return sr;
}

void World::delete_objects()
{
	for (GeometricObject * o : objects)
		delete o;
	objects.clear();
}
