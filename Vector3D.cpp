#include "Vector3D.hpp"
#include <cmath>
#include "Vector3D.hpp"
#include "Normal.hpp"

Vector3D::Vector3D()
	: x{0}, y{0}, z{0}
{}

Vector3D::Vector3D(double a)
	: x{a}, y{a}, z{a}
{}

Vector3D::Vector3D(double x, double y, double z)
	: x{x}, y{y}, z{z}
{}

Vector3D::Vector3D(Vector3D const & v)
	: x{v.x}, y{v.y}, z{v.z}
{}

Vector3D::Vector3D(Normal const & n)
	: x{n.x}, y{n.y}, z{n.z}
{}

Vector3D::Vector3D(Point3D const & p)
	: x{p.x}, y{p.y}, z{p.z}
{}

Vector3D & Vector3D::operator=(Vector3D const & rhs)
{
	if (this == &rhs)
		return *this;

	x = rhs.x;
	y = rhs.y;
	z = rhs.z;

	return *this;
}

Vector3D & Vector3D::operator=(Normal const & rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}

Vector3D & Vector3D::operator=(Point3D const & rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}

Vector3D::~Vector3D()
{}

double Vector3D::length() const
{
	return sqrt(len_squared());
}

void Vector3D::normalize()
{
	double len = length();
	x /= len;
	y /= len;
	z /= len;
}

Vector3D & Vector3D::hat()
{
	normalize();
	return *this;
}

Vector3D operator*(Matrix const & m, Vector3D const & v)
{
	return Vector3D{
		m.m[0][0] * v.x + m.m[0][1] * v.y + m.m[0][2] * v.z,
		m.m[1][0] * v.x + m.m[1][1] * v.y + m.m[1][2] * v.z,
		m.m[2][0] * v.x + m.m[2][1] * v.y + m.m[2][2] * v.z
	};
}
