#include "Matrix.hpp"

Matrix::Matrix()
{
	set_identity();
}

Matrix::Matrix(Matrix const & mat)
{
	for (int x = 0; x < 4; ++x)
		for (int y = 0; y < 4; ++y)
			m[x][y] = mat.m[x][y];
}

Matrix & Matrix::operator=(Matrix const & rhs)
{
	if (this == &rhs)
		return *this;
	
	for (int x = 0; x < 4; ++x)
		for (int y = 0; y < 4; ++y)
			m[x][y] = rhs.m[x][y];
		
	return *this;
}

Matrix::~Matrix()
{}

Matrix Matrix::operator*(Matrix const & mat) const
{
	Matrix result;
	for (int y = 0; y < 4; ++y)
	{
		for (int x = 0; x < 4; ++x)
		{
			double sum = 0.0;
			for (int j = 0; j < 4; ++j)
				sum += m[x][j] * mat.m[j][y];
			result.m[x][y] = sum;
		}
	}
	return result;
}

Matrix Matrix::operator/(double d) const
{
	Matrix result;
	for (int x = 0; x < 4; ++x)
		for (int y = 0; y < 4; ++y)
			result.m[x][y] = m[x][y] / d;
	return result;
}

void Matrix::set_identity()
{
	for (int x = 0; x < 4; ++x)
		for (int y = 0; y < 4; ++y)
			m[x][y] = (x == y) ? 1.0 : 0.0;
}
