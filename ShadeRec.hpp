#pragma once
#include "Point3D.hpp"
#include "Normal.hpp"
#include "RGBColor.hpp"

class World;

class ShadeRec
{
public:
	bool hit_an_object;  //!< did the ray hit an object ?
	Point3D local_hit_point;  //!< world coordinates of hit point on untransformed object (used for texture transformations)
	Normal normal;  //!< normal at hit point
	RGBColor color;  // used in chapter 3 only
	World & w;  // world reference for shading
	
	ShadeRec(World & wr);
	ShadeRec(ShadeRec const & sr);
	~ShadeRec();

	ShadeRec & operator=(ShadeRec const &) = delete;
}; 
