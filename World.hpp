#pragma once
#include <vector>
#include "ViewPlane.hpp"
#include "RGBColor.hpp"
#include "Tracer.hpp"
#include "GeometricObject.hpp"
#include "Sphere.hpp"
#include "Ray.hpp"

class RenderThread;

class World
{
public:
	ViewPlane vp;
	RGBColor background_color;
	Tracer * tracer_ptr;
	Sphere sphere;
	std::vector<GeometricObject *> objects;
	RenderThread * paintArea;

	World();
	~World();

	void add_object(GeometricObject * obj);
	void build();
	void render_scene() const;
	RGBColor max_to_one(RGBColor const & c) const;
	RGBColor clamp_to_color(RGBColor const & c) const;
	void display_pixel(int const row, int const column, RGBColor const & pixel_color) const;
	ShadeRec hit_bare_bones_objects(Ray const & ray);

private:
	void delete_objects();
};

inline void World::add_object(GeometricObject * obj)
{
	objects.push_back(obj);
}
