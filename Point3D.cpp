#include "Point3D.hpp"
#include <cmath>

Point3D::Point3D()
	: x{0}, y{0}, z{0}
{}

Point3D::Point3D(double a)
	: x{a}, y{a}, z{a}
{}

Point3D::Point3D(double x, double y, double z)
	: x{x}, y{y}, z{z}
{}

Point3D::Point3D(Point3D const & p)
	: x{p.x}, y{p.y}, z{p.z}
{}

Point3D & Point3D::operator=(Point3D const & rhs)
{
	if (this == &rhs)
		return *this;
	
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	
	return *this;
}

Point3D::~Point3D()
{}

double Point3D::distance(Point3D const & p) const
{
	return sqrt(d_squared(p));
}

Point3D operator*(Matrix const & m, Point3D const & p)
{
	return Point3D{
		m.m[0][0] * p.x + m.m[0][1] * p.y + m.m[0][2] * p.z + m.m[0][3],
		m.m[1][0] * p.x + m.m[1][1] * p.y + m.m[1][2] * p.z + m.m[1][3],
		m.m[2][0] * p.x + m.m[2][1] * p.y + m.m[2][2] * p.z + m.m[2][3]
	};
}
