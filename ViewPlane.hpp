#pragma once

class ViewPlane
{
public:
	int hres;  //!< horizontal image resolution
	int vres;  //!< vertical image resolution
	float s;  //!< pixel size
	float gamma;  //!< monitor gamma factor
	float inv_gamma;
	bool show_out_of_gamut;  //!< display ref if color out of gamut

	ViewPlane();
	ViewPlane(ViewPlane const & rhs);
	ViewPlane & operator=(ViewPlane const & rhs);
	~ViewPlane();

	void set_hres(int h_res);
	void set_vres(int v_res);
	void set_pixel_size(float size);
	void set_gamma(float g);
	void set_gamut_display(bool show);
};

inline void ViewPlane::set_hres(int h_res)
{
	hres = h_res;
}

inline void ViewPlane::set_vres(int v_res)
{
	vres = v_res;
}

inline void ViewPlane::set_pixel_size(float size)
{
	s = size;
}

inline void ViewPlane::set_gamma(float g)
{
	gamma = g;
	inv_gamma = 1.0 / gamma;
}

inline void ViewPlane::set_gamut_display(bool show)
{
	show_out_of_gamut = show;
}
