#pragma once
#include "Ray.hpp"
#include "RGBColor.hpp"

class World;

class Tracer
{
public:
	Tracer();
	Tracer(World * w);
	virtual ~Tracer();
	virtual RGBColor trace_ray(Ray const & ray) const;
	virtual RGBColor trace_ray(Ray const & ray, int depth) const;

protected:
	World * world_ptr;
};