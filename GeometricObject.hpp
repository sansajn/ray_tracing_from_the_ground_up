#pragma once
#include "RGBColor.hpp"
#include "Ray.hpp"
#include "ShadeRec.hpp"

class GeometricObject
{
public:
	GeometricObject();
	GeometricObject(GeometricObject const & obj);
	virtual GeometricObject * clone() const = 0;
	virtual ~GeometricObject();

	virtual bool hit(Ray const & ray, double & tmin, ShadeRec & sr) const = 0;
	
	// the following three functions are only required for chapter 3
	void set_color(RGBColor const & c);
	void set_color(float r, float g, float b);
	RGBColor get_color() const;
	
protected:
	RGBColor color;  //!< only used for bare bones ray tracing
};


inline void GeometricObject::set_color(RGBColor const & c)
{
	color = c;
}

inline void GeometricObject::set_color(float r, float g, float b)
{
	color.r = r;
	color.g = g;
	color.b = b;
}

inline RGBColor GeometricObject::get_color() const
{
	return color;
}
