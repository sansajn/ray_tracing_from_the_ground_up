#pragma once
#include "Matrix.hpp"
#include "Vector3D.hpp"
#include "Point3D.hpp"

class Normal
{
public:
	double x, y, z;
	
	Normal();
	Normal(double a);
	Normal(double x, double y, double z);
	Normal(Normal const & n);
	Normal(Vector3D const & v);
	Normal & operator=(Normal const & rhs);
	Normal & operator=(Vector3D const & rhs);
	Normal & operator=(Point3D const & rhs);
	~Normal();
	
	Normal operator-() const;  // unary minus
	Normal operator+(Normal const & n) const;
	Normal & operator+=(Normal const & n);
	double operator*(Vector3D const & v) const;  // dot product
	Normal operator*(double a) const;
	void normalize();  // convert normal to a unit normal
};

inline Normal Normal::operator-() const
{
	return Normal{-x, -y, -z};
}

inline Normal Normal::operator+(Normal const & n) const
{
	return Normal{x+n.x, y+n.y, z+n.z};
}

inline Normal & Normal::operator+=(Normal const & n)
{
	x += n.x;
	y += n.y;
	z += n.z;
	return *this;
}

inline double Normal::operator*(Vector3D const & v) const
{
	return x*v.x + y*v.y + z*v.z;
}

inline Normal Normal::operator*(double a) const
{
	return Normal{a*x, a*y, a*z};
}

//! multiplication by a double on the left
inline Normal operator*(double const a, Normal const & n)
{
	return Normal{a*n.x, a*n.y, a*n.z};
}

//! multiplication by a vector on the left
inline double operator*(Vector3D const & v, Normal const & n)
{
	return v.x*n.x + v.y*n.y + v.z*n.z;
}

//! addition of a vector on the left to return a vector
inline Vector3D operator+(Vector3D const & v, Normal const & n)
{
	return Vector3D{v.x + n.x, v.y + n.y, v.z + n.z};
}

//! subtraction of a normal from a vector to return a vector
inline Vector3D operator-(Vector3D const & v, Normal const & n)
{
	return Vector3D{v.x - n.x, v.y - n.y, v.z - n.z};
}

//! multiplication by a matrix on the left
Normal operator*(Matrix const & m, Normal const & n);
