#pragma once
#include "GeometricObject.hpp"
#include "Point3D.hpp"
#include "Normal.hpp"

class Plane : public GeometricObject
{
public:
	Plane();
	Plane(Point3D const & point, Normal const & normal);
	Plane(Plane const & plane);
	Plane * clone() const override;
	Plane & operator=(Plane const & rhs);
	~Plane();

	bool hit(Ray const & ray, double & tmin, ShadeRec & sr) const override;

private:
	Point3D a;  //!< point through which plane passes
	Normal n;  //!< normal to the plane

	static double const kEpsilon;  //!< for shadows and secondary rays
};
