#include "GeometricObject.hpp"
#include "Constants.hpp"

GeometricObject::GeometricObject()
	: color{black}
{}

GeometricObject::GeometricObject(GeometricObject const & obj)
	: color{obj.color}
{}

GeometricObject::~GeometricObject()
{}
