#include "Sphere.hpp"
#include <cmath>

double const Sphere::kEpsilon = 0.001;

Sphere::Sphere()
	: center{0}, radius{1.0}
{}

Sphere::Sphere(Point3D const & c, double r)
	: center{c}, radius{r}
{}

Sphere::Sphere(Sphere const & rhs)
	: GeometricObject{rhs}, center{rhs.center}, radius{rhs.radius}
{}

Sphere * Sphere::clone() const
{
	return new Sphere{*this};
}

Sphere & Sphere::operator=(Sphere const & rhs)
{
	if (this == &rhs)
		return *this;

	GeometricObject::operator=(rhs);
	center = rhs.center;
	radius = rhs.radius;

	return *this;
}

Sphere::~Sphere()
{}

bool Sphere::hit(Ray const & ray, double & tmin, ShadeRec & sr) const
{
	Vector3D temp = ray.o - center;
	double a = ray.d * ray.d;
	double b = 2.0 * temp * ray.d;
	double c = temp * temp - radius * radius;
	double disc = b*b - 4.0 * a * c;

	if (disc < 0)
		return false;
	else
	{
		double e = sqrt(disc);
		double denom = 2.0 * a;
		double t = (-b - e) / denom;  // smaller root

		if (t > kEpsilon)
		{
			tmin = t;
			sr.normal = (temp + t*ray.d) / radius;
			sr.local_hit_point = ray.o + t*ray.d;
			return true;
		}

		t = (-b + e) / denom;  // larger root

		if (t > kEpsilon)
		{
			tmin = t;
			sr.normal = (temp + t*ray.d) / radius;
			sr.local_hit_point = ray.o + t*ray.d;
			return true;
		}
	}

	return false;
}
