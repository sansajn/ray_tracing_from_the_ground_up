#include "World.hpp"
#include <iostream>
#include "MultipleObjects.hpp"

struct sphere_desc
{
	Point3D center;
	double r;
	RGBColor color;
};

void World::build()
{
	vp.set_hres(400);
	vp.set_vres(400);
	vp.set_pixel_size(0.5);

	background_color = RGBColor{0};
	tracer_ptr = new MultipleObjects{this};

	RGBColor yellow(1, 1, 0);
	RGBColor brown(0.71, 0.40, 0.16);
	RGBColor dark_green(0.0, 0.41, 0.41);
	RGBColor orange(1, 0.75, 0);
	RGBColor green(0, 0.6, 0.3);
	RGBColor light_green(0.65, 1, 0.30);
	RGBColor dark_yellow(0.61, 0.61, 0);
	RGBColor light_purple(0.65, 0.3, 1);
	RGBColor dark_purple(0.5, 0, 1);

	sphere_desc spheres[] = {
		{Point3D{5,3,0}, 30, yellow},
		{Point3D{45,-7,-60}, 20, brown},
		{Point3D{40,43,-100}, 17, dark_green},
		{Point3D{-20,28,-15}, 20, orange},
		{Point3D{-25,-7,-35}, 27, green},
		{Point3D{20,-27,-35}, 25, light_green},
		{Point3D{35,18,-35}, 22, green},
		{Point3D{-57,-17,-50}, 15, brown},
		{Point3D(-47, 16, -80), 23, light_green},
		{Point3D(-15, -32, -60), 22, dark_green},
		{Point3D(-35, -37, -80), 22, dark_yellow},
		{Point3D(10, 43, -80), 22, dark_yellow},
		{Point3D(30, -7, -80), 10, dark_yellow},
		{Point3D(-40, 48, -110), 18, dark_green},
		{Point3D(-10, 53, -120), 18, brown},
		{Point3D(-55, -52, -100), 10, light_purple},
		{Point3D(5, -52, -100), 15, brown},
		{Point3D(-20, -57, -120), 15, dark_purple},
		{Point3D(55, -27, -100), 17, dark_green},
		{Point3D(50, -47, -120), 15, brown},
		{Point3D(70, -42, -150), 10, light_purple},
		{Point3D(5, 73, -130), 12, light_purple},
		{Point3D(66, 21, -130), 13, dark_purple},
		{Point3D(72, -12, -140), 12, light_purple},
		{Point3D(64, 5, -160), 11, green},
		{Point3D(55, 38, -160), 12, light_purple},
		{Point3D(-73, -2, -160), 12, light_purple},
		{Point3D(30, -62, -140), 15, dark_purple},
		{Point3D(25, 63, -140), 15, dark_purple},
		{Point3D(-60, 46, -140), 15, dark_purple},
		{Point3D(-30, 68, -130), 12, light_purple},
		{Point3D(58, 56, -180), 11, green},
		{Point3D(-63, -39, -180), 11, green},
		{Point3D(46, 68, -200), 10, light_purple},
		{Point3D(-3, -72, -130), 12, light_purple}
	};

	int sphere_count = (int)(sizeof(spheres)/sizeof(spheres[0]));
	for (int i = 0; i < sphere_count; ++i)
	{
		sphere_desc & desc = spheres[i];
		Sphere * s = new Sphere{desc.center, desc.r};
		s->set_color(desc.color);
		add_object(s);
	}

	std::cout << sphere_count << std::endl;
}
