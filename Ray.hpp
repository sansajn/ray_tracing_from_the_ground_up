#pragma once

#include "Point3D.hpp"
#include "Vector3D.hpp"

class Ray
{
public:
	Point3D o;  //!< origin
	Vector3D d;  //!< direction
	
	Ray();
	Ray(Point3D const & origin, Vector3D const & dir);
	Ray(Ray const & ray);
	Ray & operator=(Ray const & rhs);
	~Ray();
};
