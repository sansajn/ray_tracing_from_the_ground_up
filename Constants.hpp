#pragma once

#include <cstdlib>  // RAND_MAX
#include "RGBColor.hpp"

double const PI = 3.1415926535897932384;
double const TWO_PI = 6.2831853071795864769;
double const PI_ON_180 = 0.0174532925199432957;
double const invPI = 0.3183098861837906715;
double const invTWO_PI = 0.1591549430918953358;

double const kEpsilon = 0.0001; 
double const kHugeValue = 1.0E10;

RGBColor const black{0.0};
RGBColor const white{1.0};
RGBColor const red{1.0, 0.0, 0.0};

float const invRAND_MAX = 1.0 / (float)RAND_MAX;
