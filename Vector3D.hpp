#pragma once
#include "Matrix.hpp"

class Normal;
class Point3D;

class Vector3D 
{
public:
	double x, y, z;
	
	Vector3D();
	Vector3D(double a);
	Vector3D(double x, double y, double z);
	Vector3D(Vector3D const & v);
	Vector3D(Normal const & n);
	Vector3D(Point3D const & p);
	Vector3D & operator=(Vector3D const & rhs);
	Vector3D & operator=(Normal const & rhs);
	Vector3D & operator=(Point3D const & rhs);
	~Vector3D();
	
	Vector3D operator-() const;  //!< unary minus
	double length() const;
	double len_squared() const;  //!< square of the length
	Vector3D operator*(double a) const;
	Vector3D operator/(double a) const;
	Vector3D operator+(Vector3D const & v) const;
	Vector3D & operator+=(Vector3D const & v);
	Vector3D operator-(Vector3D const & v) const;
	double operator*(Vector3D const & v) const;  //!< dot product
	Vector3D operator^(Vector3D const & v) const;  //!< cross product
	void normalize();  //!< convert to a unit vector
	Vector3D & hat();  //!< return a unit vector amd normalize the vector
};


inline Vector3D Vector3D::operator-() const
{
	return Vector3D{-x, -y, -z};
}

inline double Vector3D::len_squared() const
{
	return x*x + y*y + z*z;
}

inline Vector3D Vector3D::operator*(double a) const
{
	return Vector3D{a*x, a*y, a*z};
}

inline Vector3D Vector3D::operator/(double a) const
{
	return Vector3D{x/a, y/a, z/a};
}

inline Vector3D Vector3D::operator+(Vector3D const & v) const
{
	return Vector3D{x+v.x, y+v.y, z+v.z};
}

inline Vector3D & Vector3D::operator+=(Vector3D const & v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

inline Vector3D Vector3D::operator-(Vector3D const & v) const
{
	return Vector3D{x-v.x, y-v.y, z-v.z};
}

inline double Vector3D::operator*(Vector3D const & v) const
{
	return x*v.x + y*v.y + z*v.z;
}

inline Vector3D Vector3D::operator^(Vector3D const & v) const
{
	return Vector3D{
		y*v.z - z*v.y,
		z*v.x - x*v.z,
		x*v.y - y*v.x
	};
}

//! multiplication by a double on the left
inline Vector3D operator*(double a, Vector3D const & v)
{
	return Vector3D{a*v.x, a*v.y, a*v.z};
}

//! multiplication by a matrix on the left
Vector3D operator*(Matrix const & m, Vector3D const & v);
