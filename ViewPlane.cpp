#include "ViewPlane.hpp"

ViewPlane::ViewPlane()
	: hres{400}
	, vres{400}
	, s{1.0}
	, gamma{1.0}
	, inv_gamma{1.0}
	, show_out_of_gamut{false}
{}

ViewPlane::ViewPlane(ViewPlane const & rhs)
	: hres{rhs.hres}
	, vres{rhs.vres}
	, s{rhs.s}
	, gamma{rhs.gamma}
	, inv_gamma{rhs.inv_gamma}
	, show_out_of_gamut{rhs.show_out_of_gamut}
{}

ViewPlane & ViewPlane::operator=(ViewPlane const & rhs)
{
	if (this == &rhs)
		return *this;

	hres = rhs.hres;
	vres = rhs.vres;
	s = rhs.s;
	gamma = rhs.gamma;
	inv_gamma = rhs.inv_gamma;
	show_out_of_gamut = rhs.show_out_of_gamut;

	return *this;
}

ViewPlane::~ViewPlane()
{}
