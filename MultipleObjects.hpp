#pragma once
#include "Tracer.hpp"

class MultipleObjects : public Tracer
{
public:
	MultipleObjects();
	MultipleObjects(World * w);
	~MultipleObjects();

	RGBColor trace_ray(Ray const & ray) const override;
};
