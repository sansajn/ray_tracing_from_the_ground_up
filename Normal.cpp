#include "Normal.hpp"
#include <cmath>

Normal::Normal()
	: x{0}, y{0}, z{0}
{}

Normal::Normal(double a)
	: x{a}, y{a}, z{a}
{}

Normal::Normal(double x, double y, double z)
	: x{x}, y{y}, z{z}
{}

Normal::Normal(Normal const & n)
	: x{n.x}, y{n.y}, z{n.z}
{}

Normal::Normal(Vector3D const & v)
	: x{v.x}, y{v.y}, z{v.z}
{}

Normal & Normal::operator=(Normal const & rhs)
{
	if (this == &rhs)
		return *this;
	
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	
	return *this;
}

Normal & Normal::operator=(Vector3D const & rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}

Normal & Normal::operator=(Point3D const & rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}

Normal::~Normal()
{}

void Normal::normalize()
{
	double length = sqrt(x*x + y*y + z*z);
	x /= length;
	y /= length;
	z /= length;
}

Normal operator*(Matrix const & m, Normal const & n)
{
	return Normal{
		m.m[0][0] * n.x + m.m[1][0] * n.y + m.m[2][0] * n.z,
		m.m[0][1] * n.x + m.m[1][1] * n.y + m.m[2][1] * n.z,
		m.m[0][2] * n.x + m.m[1][2] * n.y + m.m[2][2] * n.z
	};
}
