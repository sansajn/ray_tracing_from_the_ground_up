// wx based rytracer skeleton implementation
#pragma once
#include <vector>
#include <cassert>
#include <wx/wx.h>
#include "World.hpp"

class RenderThread;

enum  // menu items IDs
{
	Menu_File_Quit = 100,
	Menu_File_Open,
	Menu_File_Save,
	Menu_Render_Start,
	Menu_Render_Pause,
	Menu_Render_Resume
};

class RenderCanvas : public wxScrolledWindow
{
public:
	RenderCanvas(wxWindow * parent);
	~RenderCanvas();

	void SetImage(wxImage & image);
	wxImage GetImage();

	void OnDraw(wxDC & dc);
	void renderStart();
	void renderPause();
	void renderResume();
	void OnRenderCompleted(wxCommandEvent & event);
	void OnTimerUpdate(wxTimerEvent & event);
	void OnNewPixel(wxCommandEvent & event);

protected:
	wxBitmap * m_image;
	World * w;

private:
	RenderThread * thread;
	wxStopWatch * timer;
	long pixelsRendered;
	long pixelsToRender;
	wxTimer updateTimer;

	DECLARE_EVENT_TABLE()
};

class wxraytracerFrame : public wxFrame 
{
public:
	wxraytracerFrame(wxPoint const & pos, wxSize const & size);

	void OnQuit(wxCommandEvent & event);
	void OnOpenFile(wxCommandEvent & event);
	void OnSaveFile(wxCommandEvent & event);
	void OnRenderStart(wxCommandEvent & event);
	void OnRenderCompleted(wxCommandEvent & event);
	void OnRenderPause(wxCommandEvent & event);
	void OnRenderResume(wxCommandEvent & event);

private:
	RenderCanvas * canvas;
	wxString currentPath;

	DECLARE_EVENT_TABLE()
};

class wxraytracer_app : public wxApp
{
public:
	bool OnInit() override;
	int OnExit() override;
	void SetStatusText(wxString const & text, int number = 0);

private:
	wxraytracerFrame * frame;

	DECLARE_EVENT_TABLE()
};

DECLARE_EVENT_TYPE(wxEVT_RENDER, -1)
#define ID_RENDER_COMPLETED 100
#define ID_RENDER_NEWPIXEL 101
#define ID_RENDER_UPDATE 102


class RenderPixel
{
public:
	int x, y;
	int red, green, blue;

	RenderPixel(int x, int y, int red, int green, int blue);
};

class RenderThread : public wxThread
{
public:
	RenderThread(RenderCanvas * c, World * w);
	void * Entry();
	void OnExit();
	void setPixel(int x, int y, int red, int green, int blue);

private:
	void NotifyCanvas();

	World * world;
	RenderCanvas * canvas;
	std::vector<RenderPixel *> pixels;
	wxStopWatch * timer;
	long lastUpdateTime;
};
