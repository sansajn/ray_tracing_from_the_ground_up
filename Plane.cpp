#include "Plane.hpp"

double const Plane::kEpsilon = 0.001;

Plane::Plane()
	: a{0}, n{0, 1.0, 0}
{}

Plane::Plane(Point3D const & point, Normal const & normal)
	: a{point}, n{normal}
{
	n.normalize();
}

Plane::Plane(Plane const & plane)
	: GeometricObject{plane}, a{plane.a}, n{plane.n}
{}

Plane * Plane::clone() const
{
	return new Plane{*this};
}

Plane & Plane::operator=(Plane const & rhs)
{
	if (this == &rhs)
		return *this;

	GeometricObject::operator=(rhs);
	a = rhs.a;
	n = rhs.n;

	return *this;
}

Plane::~Plane()
{}

bool Plane::hit(Ray const & ray, double & tmin, ShadeRec & sr) const
{
	float t = (a - ray.o) * n / (ray.d * n);

	if (t > kEpsilon)
	{
		tmin = t;
		sr.normal = n;
		sr.local_hit_point = ray.o + t*ray.d;
		return true;
	}

	return false;
}
